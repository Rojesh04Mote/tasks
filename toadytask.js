var students = [
  {
    name: "ram",
    class: 3,
    house: "red",
    roll: 22
  },
  {
    name: "ram",
    class: 4,
    house: "red",
    roll: 22
  },
  {
    name: "ram",
    class: 5,
    house: "green",
    roll: 22
  },
  {
    name: "ram",
    class: 5,
    house: "red",
    roll: 22
  },
  {
    name: "ram",
    class: 3,
    house: "green",
    roll: 22
  },
  {
    name: "ram",
    class: 3,
    house: "red",
    roll: 22
  }
];

function sorting(array, propertyname) {
  var obj = [];
  obj = array.reduce(function(arr, i) {
    arr[i[propertyname]] = arr[i[propertyname]] || [];
    arr[i[propertyname]].push(i);
    return arr;
  }, Object.apply(null));

  console.log(obj);
}
sorting(students, "class");

